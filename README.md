# 智慧工地系统更新

## 安装依赖
```bash
sudo pip3 install -r requirements.txt
```

## 相关路径配置  
需先修改 [config.py](config.py)  
1. `PACK_EXTENSION`: 上传的更新包扩展名列表 (目前支持 `.tar.gz`, `.tar`, `.zip`)
2. `TEMP`: 前一版本包备份路径
3. `UPDATE_PACK_STORE_PATH`: 上传和解压包存放路径
4. `UPDATE_SERVER_PATH`: 软体更新项目路径
5. `CLIENT_PATH`: 前端项目路径
6. `BACKEND_PATH`: 智慧工地项目路径

## APIs 
### 1.软体更新
#### Request
- Method: **POST**
- URL: `http://[address]:5000/api/v1/software-update/`
- Parameters:   

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| file | formData | update pack | Yes | file |
- Curl:
`curl -X POST "http://[address]:5000/api/v1/software-update/" -H "accept: application/json" -H "Content-Type: multipart/form-data" -F "file=@Archive.tar.gz"`

#### Response
- Body
```json
Success

{
  "code": 200,
  "message": "OK"
}

Fail
{
  "code": 500,
  "message": "error message"
}
```
成功上传并验证完成后更新包会在 `7` 秒后重启机器

