from flask import Flask
from apis import blueprint as api
from flask_cors import CORS

app = Flask(__name__)
app.register_blueprint(api, url_prefix='/api/v1')

# config = {
#   'ORIGINS': [
#     'http://localhost:3000', # client port
#     'http://127.0.0.1:3000',
#   ],
# }

# CORS(app, resources={ r'/*': {'origins': config['ORIGINS']}}, supports_credentials=True, crossorigin=True)
CORS(app)
@app.route('/')
def hello_world():
    return 'Wattman Smart Camera Software Update APIs. Please go to /api/v1 for more information.'

if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0")
