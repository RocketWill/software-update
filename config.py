#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: willc
"""
PACK_EXTENSION = ['.tar.gz', '.tar']                 # 压缩包扩展名

TEMP = "/home/pi/temp"                             # 前一版本包备份路径

UPDATE_PACK_STORE_PATH = "/home/pi/upload"         # 上传和解压包存放路径

UPDATE_SERVER_PATH = "/home/pi/zengxuefeng"        # 软体更新项目路径

CLIENT_PATH = "/home/pi/zengxuefeng"               # 前端项目路径

BACKEND_PATH = "/home/pi/zengxuefeng"              # 智慧工地项目路径