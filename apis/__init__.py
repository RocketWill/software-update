#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: willc
"""

from flask_restplus import Api
from flask import Blueprint
from .software_update import api as software_update_api
from .restart_machine import api as restart_machine_api


blueprint = Blueprint('api', __name__)

api = Api(blueprint)

api.add_namespace(software_update_api, path="/software-update")
api.add_namespace(restart_machine_api, path="/restart")
