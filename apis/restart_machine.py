from utils.common import restart_machine
from utils.log import log_error, log_info
from flask_restplus import Resource
from core import build_cors_prelight_response
from flask_restplus import Namespace
from flask_restplus import inputs


class RestartMachineDto:
    api = Namespace('restart', description='Restart machine.')
    restart_parser = api.parser()
    restart_parser.add_argument('restart', type=inputs.boolean, default=False)

api = RestartMachineDto.api
restart_parser = RestartMachineDto.restart_parser

@api.route('/')
class FileUpload(Resource):
    @api.expect(restart_parser)
    def post(self):
        resp = {'message': 'Cancel restart machine.', "code": 200}
        args = restart_parser.parse_args()
        try:
            if args['restart']:
                log_info("Restart machine.")
                restart_machine()
                resp['message'] = "Restart successfully."
                return resp, 200
            else:
                return resp, 200

        except Exception as e:
            resp = {'message': str(e), "code": 500}
            log_error("Failed to restart machine.")
            return resp, 500

    def options(self):
        return build_cors_prelight_response()



