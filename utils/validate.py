#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: willc
"""

def validate_update_pack(packs):
    detect_pack = False
    server_pack = False
    client_pack = False

    for pack in packs:
        if "danger_zone_monitor" in pack:
            detect_pack =  True
        elif "intelligent_monitor" in pack:
            server_pack = True
        elif "software-update" in pack:
            client_pack = True

    if detect_pack and server_pack and client_pack:
        return True
    return False
